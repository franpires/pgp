\select@language {portuges}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}
\contentsline {chapter}{\numberline {2}An\IeC {\'a}lise de requisitos}{4}
\contentsline {section}{\numberline {2.1}Requisitos funcionais e n\IeC {\~a}o funcionais}{4}
\contentsline {subsection}{\numberline {2.1.1}Requisitos funcionais}{4}
\contentsline {subsection}{\numberline {2.1.2}Requisitos n\IeC {\~a}o funcionais}{5}
\contentsline {chapter}{\numberline {3}Planeamento}{7}
\contentsline {section}{\numberline {3.1}Recursos}{7}
\contentsline {section}{\numberline {3.2}Estima\IeC {\c c}\IeC {\~a}o}{9}
\contentsline {subsection}{\numberline {3.2.1}Esfor\IeC {\c c}o dispon\IeC {\'\i }vel}{9}
\contentsline {subsection}{\numberline {3.2.2}Linhas de c\IeC {\'o}digo}{9}
\contentsline {subsection}{\numberline {3.2.3}Modelos Emp\IeC {\'\i }ricos}{10}
\contentsline {section}{\numberline {3.3}Processo de Desenvolvimento de Software}{10}
\contentsline {section}{\numberline {3.4}Gest\IeC {\~a}o de Riscos}{12}
\contentsline {chapter}{\numberline {4}Conclus\IeC {\~a}o}{15}
\contentsline {chapter}{\numberline {5}Bibliografia}{16}
